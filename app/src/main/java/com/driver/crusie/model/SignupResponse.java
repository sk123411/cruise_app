package com.driver.crusie.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by admin on 09/05/18.
 */

public class SignupResponse implements Serializable {

    @SerializedName("result")
    private boolean result;

    @SerializedName("message")
    private String message;

    @SerializedName("data")
    private UserData data;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }
}
