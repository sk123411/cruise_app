package com.driver.crusie.common;

import android.content.Context;
import android.content.SharedPreferences;

import com.driver.crusie.app.MyApplication;
import com.driver.crusie.model.UserData;
import com.google.gson.Gson;

public class AppPreferenceManager {

    private final static String PREF_FILE = "dapPref";
    private static SharedPreferences appPrefs;

    private static Context context = MyApplication.getInstance();

    private static SharedPreferences getPreferences() {
        if (appPrefs != null)
            return appPrefs;
        else
            return appPrefs = context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
    }

    public static void clearPreference() {
        appPrefs = getPreferences();
        SharedPreferences.Editor editor = appPrefs.edit();
        editor.clear();
        editor.commit();
    }

    public static void saveUser(UserData user) {
        Gson gson = new Gson();
        String json = gson.toJson(user);
        getPreferences().edit().putString(PrefConstants.USER_OBJECT, json).apply();
    }

    public static UserData getUser() {
        Gson gson = new Gson();
        String json = getPreferences().getString(PrefConstants.USER_OBJECT, "");
        return gson.fromJson(json, UserData.class);
    }

    public static void setOtpVerify(boolean otpVerify) {
        getPreferences().edit().putBoolean(PrefConstants.IS_OTP_VERIFY, otpVerify).apply();
    }

    public static boolean isOtpVerify() {
        return getPreferences().getBoolean(PrefConstants.IS_OTP_VERIFY, false);
    }

}
