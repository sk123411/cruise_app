package com.driver.crusie.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.driver.crusie.R;
import com.driver.crusie.model.MyWalletData;

import java.util.ArrayList;

public class WalletHistoryAdapter extends RecyclerView.Adapter<WalletHistoryAdapter.MyViewHolder> {
    private java.util.List<MyWalletData> List;
    View.OnClickListener onclick;
    Context context;

    public WalletHistoryAdapter(Context context, java.util.List<MyWalletData> List, View.OnClickListener onclick) {
        this.context = context;
        this.List = List;
        this.onclick = onclick;
    }

    public void filterList(ArrayList<MyWalletData> filterdNames) {
        this.List = filterdNames;
        notifyDataSetChanged();

    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTime, tvAmount;
        public TextView tvDes;
        public RelativeLayout layout;

        public MyViewHolder(View view) {
            super(view);
            tvTime = (TextView) view.findViewById(R.id.tvTime);
            layout = (RelativeLayout) view.findViewById(R.id.layout);
            tvAmount = (TextView) view.findViewById(R.id.tvAmount);
            tvDes = (TextView) view.findViewById(R.id.tvDes);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_wallet_history, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyWalletData item = List.get(position);
        if (item != null && !TextUtils.isEmpty(item.getDescription())) {
            holder.tvDes.setText(item.getDescription());
        } else {
            holder.tvDes.setText("");
        }
        if (item != null && !TextUtils.isEmpty(item.getDate_time())) {
            holder.tvTime.setText(item.getDate_time());
        } else {
            holder.tvTime.setText("");
        }
        if (item != null && !TextUtils.isEmpty(item.getAmount())) {
            double number = 0.0;
            try {
                number = Double.parseDouble(item.getAmount());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            String solution = String.format("%.1f", number);
            holder.tvAmount.setText("N" + solution);
        } else {
            holder.tvAmount.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return List.size();
    }

}