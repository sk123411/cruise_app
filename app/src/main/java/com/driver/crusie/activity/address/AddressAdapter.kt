package com.driver.crusie.activity.address

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.driver.crusie.activity.Constant
import com.driver.crusie.databinding.AddressItemBinding
import com.driver.crusie.model.SignupResponse
import io.paperdb.Paper

class AddressAdapter constructor(val address:List<AddressList.Data?>) :RecyclerView.Adapter<AddressAdapter.MyViewHolder>() {
    class MyViewHolder(view: View):RecyclerView.ViewHolder(view) {

       val binding = AddressItemBinding.bind(view)
        fun bindData(address: AddressList.Data?) {

            Paper.init(binding.root.context)
            val signupResponse = Paper.book().read<SignupResponse>(Constant.USER_DETAILS)

            binding.flatNoText.text = "Address: "+ address!!.flatNo
            binding.streetNoText.text = ""
            binding.landmarkText.text = ""
            binding.locationCode.text = "Location codes: "+address.latitude +", "+ address.longitude


            binding.deleteButton.setOnClickListener {


                AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                    .addBodyParameter("userID", signupResponse!!.data.userID)
                    .addBodyParameter("control","delete_address")
                    .addBodyParameter("addressID",address.addressID)
                    .build().getAsObject(Address::class.java, object:ParsedRequestListener<Address>{
                        override fun onResponse(response: Address?) {

                            if (response!!.result!!) {
                                AddressesActivity.addressUpdated!!.addressUpdate(true)
                            }
                        }

                        override fun onError(anError: ANError?) {


                        }

                    })




            }
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

    return MyViewHolder(AddressItemBinding.inflate(LayoutInflater.from(parent.context)).root)


    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.bindData(address.get(position))
    }

    override fun getItemCount(): Int {
    return  address.size
    }
}