package com.driver.crusie.activity;

/**
 * Created by Ravi on 29/07/15.
 */

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;
import com.androidnetworking.model.Progress;
import com.driver.crusie.R;
import com.driver.crusie.model.SignupResponse;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.function.ObjIntConsumer;

import io.paperdb.Paper;

public class FragmentDrawer extends Fragment {

    private static String TAG = FragmentDrawer.class.getSimpleName();

    private RecyclerView recyclerView;
    private TextView tvViewProfile, tvFullName;
    private ImageView ivProfileImafe;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerAdapter adapter;
    private View containerView;
    private static String[] titles = null;
    private FragmentDrawerListener drawerListener;
    private ProgressBar progressBar;

    public FragmentDrawer() {

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public static List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();
        NavDrawerItem navItem = new NavDrawerItem();
        navItem.setTitle("Payment History");
        navItem.setType("PAYMENT_HISTORY");
        navItem.setIcon(R.mipmap.i_payment);
        data.add(navItem);

        navItem = new NavDrawerItem();
        navItem.setTitle("Wallet");
        navItem.setType("WALLET");
        navItem.setIcon(R.mipmap.i_payment);
        data.add(navItem);


        navItem = new NavDrawerItem();
        navItem.setTitle("Address");
        navItem.setType("Address");
        navItem.setIcon(R.drawable.ic_address_name);
        data.add(navItem);



        navItem = new NavDrawerItem();
        navItem.setTitle("Ride History");
        navItem.setType("RIDE_HISTORY");
        navItem.setIcon(R.mipmap.icon_ride_history);
        data.add(navItem);

        navItem = new NavDrawerItem();
        navItem.setTitle("Car Hire");
        navItem.setType("CAR_HIRE");
        navItem.setIcon(R.mipmap.icon_car_rental);
        data.add(navItem);

        navItem = new NavDrawerItem();
        navItem.setTitle("Invite Friends");
        navItem.setType("INVITE_FRIENDS");
        navItem.setIcon(R.mipmap.icon_invite_friends);
        data.add(navItem);

        navItem = new NavDrawerItem();
        navItem.setTitle("Promo Code");
        navItem.setType("PROMO_CODE");
        navItem.setIcon(R.mipmap.on_sale);
        data.add(navItem);

        navItem = new NavDrawerItem();
        navItem.setTitle("Settings");
        navItem.setType("SETTINGS");
        navItem.setIcon(R.mipmap.settings);
        data.add(navItem);

        navItem = new NavDrawerItem();
        navItem.setTitle("Supports");
        navItem.setType("SUPPORTS");
        navItem.setIcon(R.mipmap.icon_support);
        data.add(navItem);

        navItem = new NavDrawerItem();
        navItem.setTitle("Logout");
        navItem.setType("LOGOUT");
        navItem.setIcon(R.mipmap.i_logout);
        data.add(navItem);
        /*// preparing navigation drawer items
        for (int i = 0; i < titles.length; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setTitle(titles[i]);
            data.add(navItem);
        }*/
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // drawer labels
        titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        recyclerView = layout.findViewById(R.id.drawerList);
        tvViewProfile = layout.findViewById(R.id.tvViewProfile);
        tvFullName = layout.findViewById(R.id.ivFullName);
        ivProfileImafe = layout.findViewById(R.id.ivProfile);
        progressBar = layout.findViewById(R.id.loadingBar);


        Paper.init(requireActivity());
        SignupResponse signupResponse = Paper.book().read(Constant.Companion.getUSER_DETAILS());



        getProfile(signupResponse.getData().getUserID());



        tvViewProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ViewProfileActivity.class));
            }
        });





        adapter = new NavigationDrawerAdapter(getActivity(), getData());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {




                drawerListener.onDrawerItemSelected(view, position);




                mDrawerLayout.closeDrawer(containerView);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return layout;
    }


    public void getProfile(String userID) {

        progressBar.setVisibility(View.VISIBLE);


        AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                .addBodyParameter("control","update_profile_info")
                .addBodyParameter("userID",userID)
                .build().getAsObject(SignupResponse.class
                , new ParsedRequestListener<SignupResponse>() {
                    @Override
                    public void onResponse(SignupResponse response) {
                        if (response.isResult()){
                            progressBar.setVisibility(View.GONE);

                            Toast.makeText(getContext(),"Profile updated",Toast.LENGTH_SHORT).show();



                            tvFullName.setText(response.getData().getName() + " "+response.getData().getSurname());

                            Picasso.get().load(response.getData().getPath() +
                                    response.getData().getImage())
                                    .placeholder(R.drawable.app_logo).into(ivProfileImafe);




                        }else {
                            progressBar.setVisibility(View.GONE);

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressBar.setVisibility(View.GONE);

                    }
                });

    }



    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }
}
