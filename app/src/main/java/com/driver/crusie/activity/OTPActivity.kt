package com.driver.crusie.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.driver.crusie.R
import com.driver.crusie.app.MyApplication
import com.driver.crusie.common.AppPreferenceManager
import com.driver.crusie.common.Utils
import com.driver.crusie.databinding.ActivityLoginBinding
import com.driver.crusie.databinding.ActivityOtpBinding
import com.driver.crusie.model.SignupResponse
import io.paperdb.Paper


class OTPActivity : AppCompatActivity() {

    var edtOtp: EditText? = null
    var ibNext: ImageButton? = null
    lateinit var binding: ActivityOtpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOtpBinding.inflate(layoutInflater)

        setContentView(binding.root)
        ibNext = findViewById(R.id.ibNext)
        edtOtp = findViewById(R.id.edtOtp)
        ibNext!!.setOnClickListener {
            if (TextUtils.isEmpty(edtOtp!!.text.toString().trim())) {
                Utils.showToast(this, "Please enter OTP.")
            } else {
               // callPanCardPostApi(edtOtp!!.text.toString().trim())
            }
        }



        binding.ibNext.setOnClickListener {


            if(binding.edtOtp.text.isEmpty()){


                binding.edtOtp.setError("Enter otp")
            }else {


                Paper.init(applicationContext)

                val response = Paper.book().read<SignupResponse>(Constant.USER_DETAILS)



                if (binding.edtOtp.text.toString().equals(response.data.otp)){




                    otpVerifyProcess(response)












                }else {

                    Toast.makeText(applicationContext,"OTP not matched", Toast.LENGTH_SHORT).show()

                }






            }


        }







    }

//    private fun callPanCardPostApi(OTP: String) {
//        //mTempPhotoPath
//        if (Utils.isInternetAvailable(this)) {
//            //rlProgress!!.visibility = View.VISIBLE
//            var control = "otp_verify"
//            MyApplication.apiServicePost.otpVerify(
//                control,
//                AppPreferenceManager.getUser().mobile,
//                AppPreferenceManager.getUser().userID,
//                OTP
//            )
//                .enqueue(object : Callback<SignupResponse> {
//                    override fun onResponse(
//                        call: Call<SignupResponse>,
//                        response: Response<SignupResponse>
//                    ) {
//                        //rlProgress!!.visibility = View.GONE
//                        val statusCode = response.code()
//                        if (statusCode == 200) {
//                            if (response.body()!!.message != null && !TextUtils.isEmpty(response.body()!!.message)) {
//                                Utils.showToast(this@OTPActivity, response.body()!!.message)
//                            }
//                            if (response.body()!!.data != null) {
//                                AppPreferenceManager.setOtpVerify(true)
//                                AppPreferenceManager.saveUser(response.body()!!.data)
//                                startActivity(Intent(this@OTPActivity, HomeActivity::class.java))
//                                finish()
//                            }
//
//
//                        }
//                    }
//
//                    override fun onFailure(call: Call<SignupResponse>, t: Throwable) {
//                        //rlProgress!!.visibility = View.GONE
//                        Log.e("TAGArtalent", t.toString())
//                    }
//                })
//        }
//    }



    fun otpVerifyProcess(signupResponse: SignupResponse) {




            AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                .addBodyParameter("control", "otp_verify")
                .addBodyParameter("mobile", signupResponse.data.mobile)
                .addBodyParameter("userID", signupResponse.data.userID)
                .addBodyParameter("otp", binding.edtOtp.text.toString())

//                .addBodyParameter("password",binding.etPassword.text.toString())
                .build().getAsObject(SignupResponse::class.java, object:
                    ParsedRequestListener<SignupResponse> {
                    override fun onResponse(response: SignupResponse?) {

                        if (response!!.isResult){



                            Toast.makeText(applicationContext,"OTP Verified successfully", Toast.LENGTH_SHORT).show()


                            startActivity(Intent(applicationContext,HomeActivity::class.java))


                        }else {
                            Toast.makeText(applicationContext,""+response.message, Toast.LENGTH_SHORT).show()


                        }
                    }

                    override fun onError(anError: ANError?) {
                    }

                })










    }

}