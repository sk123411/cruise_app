package com.driver.crusie.activity

import android.Manifest
import android.app.Activity
import android.content.ContentUris
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.driver.crusie.R
import com.driver.crusie.databinding.ActivityViewProfileBinding
import com.driver.crusie.model.SignupResponse
import com.github.dhaval2404.imagepicker.ImagePicker
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import com.squareup.picasso.Picasso
import io.paperdb.Paper
import java.io.File

class ViewProfileActivity : AppCompatActivity() {


    lateinit var binding: ActivityViewProfileBinding
    var datafield:DATAFIELD?=null
    var signupResponse:SignupResponse?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityViewProfileBinding.inflate(layoutInflater)

        setContentView(binding.root)
        setupToolbarBack("")

        AndroidNetworking.initialize(applicationContext)

        Paper.init(applicationContext)
        signupResponse = Paper.book().read<SignupResponse>(Constant.USER_DETAILS,null)



        if (signupResponse==null){


        }else {
            getProfile(signupResponse!!.data.userID)
        }



















        binding.editNameButton.setOnClickListener {



            if (!binding.changeFNameEdit.isEnabled) {
                binding.changeFNameEdit.isEnabled = true;
                binding.changeFNameEdit.requestFocus()
                binding.changeFNameEdit.setText("")

            }else {



                    if (!checkEmptyText(binding.changeFNameEdit)) {


                        updateProfile(DATAFIELD.NAME, binding.changeFNameEdit.text.toString())

                        binding.changeFNameEdit.isEnabled = false;

                    }




            }

        }



        binding.editSurnameButton.setOnClickListener {

            if (!binding.changeLNameEdit.isEnabled) {
                binding.changeLNameEdit.isEnabled = true;
                binding.changeLNameEdit.requestFocus()
                binding.changeLNameEdit.setText("")

            }else {



                if (!checkEmptyText(binding.changeLNameEdit)) {


                    updateProfile(DATAFIELD.SURNAME, binding.changeLNameEdit.text.toString())

                    binding.changeLNameEdit.isEnabled = false;

                }




            }


        }


        binding.changeEmailButton.setOnClickListener {

            if (!binding.emailEditText.isEnabled) {
                binding.emailEditText.isEnabled = true;
                binding.emailEditText.requestFocus()
                binding.emailEditText.setText("")

            }else {



                if (!checkEmptyText(binding.emailEditText)) {


                    updateProfile(DATAFIELD.EMAIL, binding.emailEditText.text.toString())

                    binding.emailEditText.isEnabled = false;

                }




            }



        }


        binding.userProfileImage.setOnClickListener {

            Dexter.withContext(applicationContext)
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(object:MultiplePermissionsListener{
                    override fun onPermissionsChecked(p0: MultiplePermissionsReport?) {
                        ImagePicker.with(this@ViewProfileActivity)
                            .crop()	    			//Crop image(Optional), Check Customization for more option
                            .compress(1024)			//Final image size will be less than 1 MB(Optional)
                            .maxResultSize(1080, 1080)	//Final image resolution will be less than 1080 x 1080(Optional)
                            .start()
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        p0: MutableList<PermissionRequest>?,
                        p1: PermissionToken?
                    ) {

                    }

                }).check()




        }



        binding.changeMobileButton.setOnClickListener {

            if (!binding.editMobile.isEnabled) {
                binding.editMobile.isEnabled = true;
                binding.editMobile.requestFocus()
                binding.editMobile.setText("")

            }else {



                if (!checkEmptyText(binding.editMobile)) {


                    updateProfile(DATAFIELD.MOBILE, binding.editMobile.text.toString())

                    binding.editMobile.isEnabled = false;

                }




            }



        }




        binding.chnageBirthButton.setOnClickListener {

            if (!binding.dobEditText.isEnabled) {
                binding.dobEditText.isEnabled = true;
                binding.dobEditText.requestFocus()
                binding.dobEditText.setText("")

            }else {



                if (!checkEmptyText(binding.dobEditText)) {


                    updateProfile(DATAFIELD.DOB, binding.dobEditText.text.toString())

                    binding.dobEditText.isEnabled = false;

                }




            }

        }
    }

    private fun getProfile(userID: String?) {

        binding.loadingBar.visibility = View.VISIBLE

        AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
            .addBodyParameter("control","update_profile_info")
            .addBodyParameter("userID",userID)
            .build().getAsObject(SignupResponse::class.java,object:ParsedRequestListener<SignupResponse>{
                override fun onResponse(response: SignupResponse?) {
                    binding.loadingBar.visibility = View.GONE


                    if (response!!.isResult){

                        Toast.makeText(applicationContext,"Profile updated",Toast.LENGTH_SHORT).show()


                        binding.changeFNameEdit.setText(response.data.name)
                        binding.changeLNameEdit.setText(response.data.surname)
                        binding.dobEditText.setText(response.data.dob)
                        binding.emailEditText.setText(response.data.email)
                        binding.emailText.setText(response.data.email)
                        binding.editMobile.setText(response.data.mobile)

                        Picasso.get().load(response.data.path + response.data.image)
                            .placeholder(R.drawable.app_logo).into(binding.userProfileImage)



                    }


                }

                override fun onError(anError: ANError?) {
                    binding.loadingBar.visibility = View.GONE

                    Toast.makeText(applicationContext,"Error "+anError.toString(),Toast.LENGTH_SHORT).show()

                }

            })

    }

    private fun updateProfile(data: DATAFIELD, field:String?) {

        binding.loadingBar.visibility = View.VISIBLE



        when(data){

            DATAFIELD.NAME -> {



                AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                    .addBodyParameter("control","update_profile_info")
                    .addBodyParameter(DATAFIELD.NAME.name.toLowerCase(),field)
                    .addBodyParameter("userID", signupResponse!!.data.userID)
                    .build().getAsObject(SignupResponse::class.java,object:ParsedRequestListener<SignupResponse>{
                        override fun onResponse(response: SignupResponse?) {

                            if (response!!.isResult){
                                binding.loadingBar.visibility = View.GONE

                                Toast.makeText(applicationContext,"Profile updated",Toast.LENGTH_SHORT).show()

                            }else {
                                binding.loadingBar.visibility = View.GONE

                            }


                        }

                        override fun onError(anError: ANError?) {
                            binding.loadingBar.visibility = View.GONE

                            Toast.makeText(applicationContext,"Error "+anError.toString(),Toast.LENGTH_SHORT).show()

                        }

                    })
            }

            DATAFIELD.SURNAME -> {


                AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                    .addBodyParameter("control","update_profile_info")
                    .addBodyParameter(DATAFIELD.SURNAME.name.toLowerCase(),field)
                    .addBodyParameter("userID", signupResponse!!.data.userID)

                    .build().getAsObject(SignupResponse::class.java,object:ParsedRequestListener<SignupResponse>{
                        override fun onResponse(response: SignupResponse?) {

                            if (response!!.isResult){
                                binding.loadingBar.visibility = View.GONE

                                Toast.makeText(applicationContext,"Profile updated",Toast.LENGTH_SHORT).show()

                            }else {
                                binding.loadingBar.visibility = View.GONE

                            }


                        }

                        override fun onError(anError: ANError?) {
                            binding.loadingBar.visibility = View.GONE

                            Toast.makeText(applicationContext,"Error "+anError.toString(),Toast.LENGTH_SHORT).show()

                        }

                    })
            }

            DATAFIELD.MOBILE -> {

                AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                    .addBodyParameter("control","update_profile_info")
                    .addBodyParameter(DATAFIELD.MOBILE.name.toLowerCase(),field)
                    .addBodyParameter("userID", signupResponse!!.data.userID)

                    .build().getAsObject(SignupResponse::class.java,object:ParsedRequestListener<SignupResponse>{
                        override fun onResponse(response: SignupResponse?) {

                            if (response!!.isResult){
                                binding.loadingBar.visibility = View.GONE

                                Toast.makeText(applicationContext,"Profile updated",Toast.LENGTH_SHORT).show()

                            }else {

                                binding.loadingBar.visibility = View.GONE

                            }


                        }

                        override fun onError(anError: ANError?) {
                            binding.loadingBar.visibility = View.GONE

                            Toast.makeText(applicationContext,"Error "+anError.toString(),Toast.LENGTH_SHORT).show()

                        }

                    })
            }

            DATAFIELD.EMAIL -> {

                AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                    .addBodyParameter("control","update_profile_info")
                    .addBodyParameter(DATAFIELD.EMAIL.name.toLowerCase(),field)
                    .addBodyParameter("userID", signupResponse!!.data.userID)

                    .build().getAsObject(SignupResponse::class.java,object:ParsedRequestListener<SignupResponse>{
                        override fun onResponse(response: SignupResponse?) {

                            if (response!!.isResult){
                                binding.loadingBar.visibility = View.GONE

                                Toast.makeText(applicationContext,"Profile updated",Toast.LENGTH_SHORT).show()

                            }else {

                                binding.loadingBar.visibility = View.GONE

                            }


                        }

                        override fun onError(anError: ANError?) {
                            binding.loadingBar.visibility = View.GONE

                            Toast.makeText(applicationContext,"Error "+anError.toString(),Toast.LENGTH_SHORT).show()

                        }

                    })
            }

            DATAFIELD.DOB -> {

                AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                    .addBodyParameter("control","update_profile_info")
                    .addBodyParameter(DATAFIELD.DOB.name.toLowerCase(),field)
                    .addBodyParameter("userID", signupResponse!!.data.userID)

                    .build().getAsObject(SignupResponse::class.java,object:ParsedRequestListener<SignupResponse>{
                        override fun onResponse(response: SignupResponse?) {

                            if (response!!.isResult){
                                binding.loadingBar.visibility = View.GONE

                                Toast.makeText(applicationContext,"Profile updated",Toast.LENGTH_SHORT).show()

                            }else {
                                binding.loadingBar.visibility = View.GONE

                            }


                        }

                        override fun onError(anError: ANError?) {
                            binding.loadingBar.visibility = View.GONE

                            Toast.makeText(applicationContext,"Error "+anError.toString(),Toast.LENGTH_SHORT).show()

                        }

                    })
            }



        }









    }


    var toolbar: Toolbar? = null
    var tvTitle: TextView? = null
    fun setupToolbarBack(title: String?) {
        toolbar = findViewById(R.id.toolbar)
        tvTitle = findViewById(R.id.tvTitle)
        if (toolbar != null && tvTitle != null) {
            setSupportActionBar(toolbar)
            if (supportActionBar != null) {
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                tvTitle!!.setText(title)
                toolbar!!.setNavigationIcon(R.mipmap.i_back_green)
            }
        }
    }



    fun changeProfileImage(file: File?){

        binding.loadingBar.visibility = View.VISIBLE


        AndroidNetworking.upload("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
            .addMultipartParameter("control","update_profile_image")
            .addMultipartFile("image",file)
            .addMultipartParameter("userID", signupResponse!!.data.userID)

            .build().getAsObject(SignupResponse::class.java,object:ParsedRequestListener<SignupResponse>{
                override fun onResponse(response: SignupResponse?) {

                    if (response!!.isResult){
                        binding.loadingBar.visibility = View.GONE

                        Toast.makeText(applicationContext,"Profile updated",Toast.LENGTH_SHORT).show()

                    }else {
                        binding.loadingBar.visibility = View.GONE

                    }


                }

                override fun onError(anError: ANError?) {
                    binding.loadingBar.visibility = View.GONE

                    Toast.makeText(applicationContext,"Error "+anError.toString(),Toast.LENGTH_SHORT).show()

                }

            })
    }



    fun checkEmptyText(editText: EditText):Boolean{

        if (editText.text.toString().isEmpty()&&editText.id==R.id.changeFNameEdit){

            editText.setError("Please enter first name")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.changeLNameEdit){

            editText.setError("Please enter last name")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.emailEditText){

            editText.setError("Please enter email address")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.dobEditText){

            editText.setError("Please enter date of birth")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.editMobile){

            editText.setError("Please enter mobile")
            return true
        }

        return false



    }


    enum class DATAFIELD{
        NAME,SURNAME,EMAIL,MOBILE,DOB


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            val uri: Uri = data?.data!!

            // Use Uri object instead of File to avoid storage permissions
          //  binding.userProfileImage.setImageURI(uri)

//            FileCon
//
//            uri?.let {
//
//                val imageFile = File(uri.path!!)
//                changeProfileImage(imageFile)
//
//            }






        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show()
        }

    }


}