package com.driver.crusie.activity.address

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.driver.crusie.R
import com.driver.crusie.activity.Constant
import com.driver.crusie.databinding.ActivityAddressesBinding
import com.driver.crusie.databinding.AddAddressLytBinding
import com.driver.crusie.model.SignupResponse
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.bottomsheet.BottomSheetDialog
import io.paperdb.Paper
import java.lang.invoke.ConstantCallSite
import java.util.*
import kotlin.math.sign


class AddressesActivity : AppCompatActivity() {

    lateinit var binding: ActivityAddressesBinding
    private val AUTOCOMPLETE_REQUEST_CODE = 5667
    var signupResponse: SignupResponse? = null
    var fields: List<Place.Field> =
        Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityAddressesBinding.inflate(layoutInflater)
        setContentView(binding.root)
        Places.initialize(applicationContext, resources.getString(R.string.places_api_key))
        val placesClient: PlacesClient = Places.createClient(applicationContext)


        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val title = toolbar.findViewById<TextView>(R.id.tvTitle)

        title.setText("Address")


        Paper.init(applicationContext)
        signupResponse = Paper.book().read(Constant.USER_DETAILS)


        binding.addressButton.setOnClickListener {
          //  startPlacePicker()

            openAddAddressDialog()
        }

        getAddress()



        AddressesActivity.addressUpdated = object :AddressUpdated{
            override fun addressUpdate(updated: Boolean) {

                getAddress()

            }

        }



    }

    private fun openAddAddressDialog() {
       val bottomSheetDialog = BottomSheetDialog(this)
        val binding = AddAddressLytBinding.inflate(bottomSheetDialog.layoutInflater)
        bottomSheetDialog.setContentView(binding.root)
        bottomSheetDialog.show()

        var addressType:String?=""

        binding.addressTypeGroup.setOnCheckedChangeListener(object:RadioGroup.OnCheckedChangeListener{
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {

                if (checkedId==R.id.radioHome){

                    addressType = "Home"
                }else {
                    addressType = "Office"

                }
            }

        })




        binding.addAddressButton.setOnClickListener {
            addAddress(binding.flatNoEdit.text.toString() +", "+binding.streetNameEdit.text.toString() +", "+binding.landMarkEdit.text.toString(),
            binding.latitude.text.toString(),binding.longitude.text.toString(),addressType!!)
            bottomSheetDialog.dismiss()

            getAddress()


        }



    }


    fun startPlacePicker() {


        val intent = Autocomplete.IntentBuilder(AutocompleteActivityMode.FULLSCREEN, fields)
            .build(applicationContext)
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE)


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (data != null) {

            if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
                if (resultCode == Activity.RESULT_OK) {
                    val place = Autocomplete.getPlaceFromIntent(data)



//                    addAddress(
//                        place.address,
//                        place.latLng!!.latitude.toString(),
//                        place.latLng!!.longitude.toString()
//                    )


                }


            }

        }else if (resultCode == AutocompleteActivity.RESULT_ERROR) {


            Toast.makeText(getApplicationContext(),"Error picking up location",Toast.LENGTH_SHORT).show();

        } else if (resultCode == Activity.RESULT_CANCELED) {
            // The user canceled the operation.
            Toast.makeText(getApplicationContext(),"Task Cancelled",Toast.LENGTH_SHORT).show();

        }

    }

    fun getAddress() {
        binding.loadingbar.visibility = View.VISIBLE

        AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
            .addBodyParameter("userID", signupResponse!!.data.userID)
            .addBodyParameter("control","show_all_address")
            .build().getAsObject(AddressList::class.java, object : ParsedRequestListener<AddressList> {
                override fun onResponse(response: AddressList?) {

                    if (response!!.result!!) {

                        binding.loadingbar.visibility = View.GONE

                        Log.d("ADDDRESSSS", ":::"+response.toString())
                        setUpAdapter(response.data!!)


                    }else {
                        binding.loadingbar.visibility = View.GONE

                    }


                }

                override fun onError(anError: ANError?) {
                    binding.loadingbar.visibility = View.GONE

                    Toast.makeText(getApplicationContext(),""+anError.toString(),Toast.LENGTH_SHORT).show();

                }

            })


    }

    private fun setUpAdapter(list: List<AddressList.Data?>) {

        binding.addressList.apply {
            layoutManager = LinearLayoutManager(applicationContext)
            adapter = AddressAdapter(list)

        }
    }

    fun addAddress(address: String?, latitude: String, longitude: String,type:String) {

        AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
            .addBodyParameter("control","add_address")
            .addBodyParameter("flat_no", address)
            .addBodyParameter("street_name",address)
            .addBodyParameter("latitude", latitude)
            .addBodyParameter("longitude", longitude)
            .addBodyParameter("userID", signupResponse!!.data.userID)
            .addBodyParameter("type",type)
            .build().getAsObject(Address::class.java, object : ParsedRequestListener<Address> {
                override fun onResponse(response: Address?) {

                    if (response!!.result!!) {

                        Toast.makeText(
                            applicationContext,
                            "Address added successfully",
                            Toast.LENGTH_LONG
                        ).show()

                    }


                }

                override fun onError(anError: ANError?) {


                }

            })


    }

    companion object {

        var addressUpdated:AddressUpdated?=null

    }

}

