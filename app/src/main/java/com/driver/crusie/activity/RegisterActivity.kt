package com.driver.crusie.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.driver.crusie.R
import com.driver.crusie.databinding.ActivityRegisterBinding
import com.driver.crusie.model.SignupResponse

class RegisterActivity : AppCompatActivity() {


    lateinit var binding:ActivityRegisterBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        AndroidNetworking.initialize(applicationContext)


        setContentView(binding.root)
        //setupToolbarBack("")


        binding.registerButton.setOnClickListener {

            startRegisterProcess()
        }

    }

    fun startRegisterProcess() {

        if (!checkEmptyText(binding.eTfirstName)&&!checkEmptyText(binding.eTlastName)&&!checkEmptyText(binding.eTmobileNumber)&&!checkEmptyText(binding.etEmailAddress)
            &&!checkEmptyText(binding.etPassword)){



            AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                .addBodyParameter("control", "driver_signup")
                .addBodyParameter("name", binding.eTfirstName.text.toString())
                .addBodyParameter("surname", binding.eTlastName.text.toString())
                .addBodyParameter("email",binding.etEmailAddress.text.toString())
                .addBodyParameter("mobile", binding.eTmobileNumber.text.toString())
//                .addBodyParameter("password",binding.etPassword.text.toString())
                .build().getAsObject(SignupResponse::class.java, object:
                    ParsedRequestListener<SignupResponse> {
                    override fun onResponse(response: SignupResponse?) {

                        if (response!!.isResult){



                            Toast.makeText(applicationContext,"Registration successfull", Toast.LENGTH_SHORT).show()


                            startActivity(Intent(applicationContext,LoginActivity::class.java))


                        }else {
                            Toast.makeText(applicationContext,""+response.message, Toast.LENGTH_SHORT).show()


                        }
                    }

                    override fun onError(anError: ANError?) {
                    }

                })



        }






    }





    fun checkEmptyText(editText: EditText):Boolean{

        if (editText.text.toString().isEmpty()&&editText.id==R.id.eTfirstName){

            editText.setError("Please enter first name")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.eTlastName){

            editText.setError("Please enter last name")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.etEmailAddress){

            editText.setError("Please enter email address")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.eTmobileNumber){

            editText.setError("Please enter mobile number")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.etPassword){

            editText.setError("Please enter password")
            return true
        }

        return false



    }

    /*var toolbar: Toolbar? = null
    var tvTitle: TextView? = null
    fun setupToolbarBack(title: String?) {
        toolbar = findViewById(R.id.toolbar)
        tvTitle = findViewById(R.id.tvTitle)
        if (toolbar != null && tvTitle != null) {
            setSupportActionBar(toolbar)
            if (supportActionBar != null) {
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                tvTitle!!.setText(title)
                toolbar!!.setNavigationIcon(R.mipmap.i_back_green)
            }
        }
    }*/
}