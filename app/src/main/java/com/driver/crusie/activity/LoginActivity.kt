package com.driver.crusie.activity

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.ParsedRequestListener
import com.driver.crusie.R
import com.driver.crusie.app.MyApplication
import com.driver.crusie.common.AppPreferenceManager
import com.driver.crusie.common.Utils
import com.driver.crusie.databinding.ActivityLoginBinding
import com.driver.crusie.databinding.ActivityRegisterBinding
import com.driver.crusie.model.SignupResponse
import io.paperdb.Paper


class LoginActivity : AppCompatActivity() {
    var tvNext: TextView? = null
    var edtMobile: EditText? = null
    var tvCreateMyAccount: TextView? = null

    lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)

        setContentView(binding.root)
        tvNext = findViewById(R.id.registerButton)
        tvCreateMyAccount = findViewById(R.id.tvCreateMyAccount)
        tvCreateMyAccount!!.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        edtMobile = findViewById(R.id.edtMobile)
        tvNext!!.setOnClickListener {
            //
            if (TextUtils.isEmpty(edtMobile!!.text.toString().trim())) {
                Utils.showToast(this, "Please enter mobile no.")
            } else {
               // callPanCardPostApi(edtMobile!!.text.toString().trim())
            }

        }


        binding.registerButton.setOnClickListener {
            startRegisterProcess()

        }

    }



    fun startRegisterProcess() {

        if (!checkEmptyText(binding.edtPassword)&&!checkEmptyText(binding.editEmail)&&!checkEmptyText(binding.edtMobile)){



            AndroidNetworking.post("https://maestros.co.in/Cruise_Taxi/appservice/process.php")
                .addBodyParameter("control", "driver_signup")
                .addBodyParameter("email",binding.editEmail.text.toString())
                .addBodyParameter("mobile", binding.edtMobile.text.toString())
//                .addBodyParameter("password",binding.etPassword.text.toString())
                .build().getAsObject(SignupResponse::class.java, object:
                    ParsedRequestListener<SignupResponse> {
                    override fun onResponse(response: SignupResponse?) {

                        if (response!!.isResult){


                            Paper.init(applicationContext)
                            Paper.book().write(Constant.USER_DETAILS, response!!)


                            Toast.makeText(applicationContext,"Registration successfull", Toast.LENGTH_SHORT).show()


                            startActivity(Intent(applicationContext,OTPActivity::class.java))


                        }else {
                            Toast.makeText(applicationContext,""+response.message, Toast.LENGTH_SHORT).show()


                        }
                    }

                    override fun onError(anError: ANError?) {
                    }

                })



        }






    }

    fun checkEmptyText(editText: EditText):Boolean{

        if (editText.text.toString().isEmpty()&&editText.id==R.id.eTfirstName){

            editText.setError("Please enter first name")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.eTlastName){

            editText.setError("Please enter last name")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.etEmailAddress){

            editText.setError("Please enter email address")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.eTmobileNumber){

            editText.setError("Please enter mobile number")
            return true
        }else if (editText.text.toString().isEmpty()&&editText.id==R.id.etPassword){

            editText.setError("Please enter password")
            return true
        }

        return false



    }





//    private fun callPanCardPostApi(mobile: String) {
//        //mTempPhotoPath
//        if (Utils.isInternetAvailable(this)) {
//            //rlProgress!!.visibility = View.VISIBLE
//            var control = "signup"
//            MyApplication.apiServicePost.signup(
//                control, mobile
//            )
//                .enqueue(object : Callback<SignupResponse> {
//                    override fun onResponse(
//                        call: Call<SignupResponse>,
//                        response: Response<SignupResponse>
//                    ) {
//                        //rlProgress!!.visibility = View.GONE
//                        val statusCode = response.code()
//                        if (statusCode == 200) {
//                            if (response.body()!!.message != null && !TextUtils.isEmpty(response.body()!!.message)) {
//                                Utils.showToast(this@LoginActivity, response.body()!!.message)
//                            }
//                            if (response.body()!!.data != null) {
//                                AppPreferenceManager.saveUser(response.body()!!.data)
//                                startActivity(Intent(this@LoginActivity, OTPActivity::class.java))
//                                finish()
//                            }
//
//
//                        }
//                    }
//
//                    override fun onFailure(call: Call<SignupResponse>, t: Throwable) {
//                        //rlProgress!!.visibility = View.GONE
//                        Log.e("TAGArtalent", t.toString())
//                    }
//                })
//        }
//    }
}