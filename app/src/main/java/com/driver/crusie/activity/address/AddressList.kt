package com.driver.crusie.activity.address


import com.google.gson.annotations.SerializedName

data class AddressList(
    @SerializedName("data")
    val `data`: List<Data?>?,
    @SerializedName("message")
    val message: String?,
    @SerializedName("result")
    val result: Boolean?
) {
    data class Data(
        @SerializedName("addressID")
        val addressID: String?,
        @SerializedName("flat_no")
        val flatNo: String?,
        @SerializedName("landmark")
        val landmark: String?,
        @SerializedName("latitude")
        val latitude: String?,
        @SerializedName("longitude")
        val longitude: String?,
        @SerializedName("path")
        val path: String?,
        @SerializedName("street_name")
        val streetName: String?,
        @SerializedName("type")
        val type: String?,
        @SerializedName("userID")
        val userID: String?
    )
}