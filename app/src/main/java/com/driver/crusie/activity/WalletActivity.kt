package com.driver.crusie.activity

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.driver.crusie.R
import com.driver.crusie.adapter.WalletHistoryAdapter
import com.driver.crusie.model.MyWalletData
import java.util.ArrayList

class WalletActivity : AppCompatActivity(), View.OnClickListener {


    var toolbar: Toolbar? = null
    var tvTitle: TextView? = null
    var recyclerView: RecyclerView? = null
    private val dataList: MutableList<MyWalletData> = ArrayList()
    private var mAdapter: WalletHistoryAdapter? = null
    var swipeToRefresh: SwipeRefreshLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_wallet)
        setupToolbarBack("Wallet")
        recyclerView = findViewById(R.id.recyclerview)
        swipeToRefresh = findViewById(R.id.swipeToRefresh)
        setAdapter()
        swipeToRefresh!!.setOnRefreshListener {
            swipeToRefresh!!.isRefreshing = false
        }
    }

    fun setupToolbarBack(title: String?) {
        toolbar = findViewById(R.id.toolbar)
        tvTitle = findViewById(R.id.tvTitle)
        if (toolbar != null && tvTitle != null) {
            setSupportActionBar(toolbar)
            if (supportActionBar != null) {
                supportActionBar!!.setDisplayShowTitleEnabled(false)
                tvTitle!!.setText(title)
                toolbar!!.setNavigationIcon(R.mipmap.i_back_green)
            }
        }
    }

    private fun setAdapter() {
        var myWalletData = MyWalletData()
        myWalletData.amount = "101.00"
        myWalletData.type = "CREDIT"
        myWalletData.description = "Added to Wallet"
        myWalletData.date_time = "1 Feb'19 . #123456"
        dataList.add(myWalletData)

        myWalletData = MyWalletData()
        myWalletData.amount = "101.00"
        myWalletData.type = "CREDIT"
        myWalletData.description = "Added to Wallet"
        myWalletData.date_time = "1 Feb'19 . #123456"
        dataList.add(myWalletData)

        myWalletData = MyWalletData()
        myWalletData.amount = "101.00"
        myWalletData.type = "CREDIT"
        myWalletData.description = "Added to Wallet"
        myWalletData.date_time = "1 Feb'19 . #123456"
        dataList.add(myWalletData)

        myWalletData = MyWalletData()
        myWalletData.amount = "101.00"
        myWalletData.type = "CREDIT"
        myWalletData.description = "Added to Wallet"
        myWalletData.date_time = "1 Feb'19 . #123456"
        dataList.add(myWalletData)

        myWalletData = MyWalletData()
        myWalletData.amount = "101.00"
        myWalletData.type = "CREDIT"
        myWalletData.description = "Added to Wallet"
        myWalletData.date_time = "1 Feb'19 . #123456"
        dataList.add(myWalletData)

        myWalletData = MyWalletData()
        myWalletData.amount = "101.00"
        myWalletData.type = "CREDIT"
        myWalletData.description = "Added to Wallet"
        myWalletData.date_time = "1 Feb'19 . #123456"
        dataList.add(myWalletData)

        mAdapter = WalletHistoryAdapter(this, dataList, this)
        val gridLayoutManager = LinearLayoutManager(this)
        recyclerView!!.layoutManager = gridLayoutManager
        recyclerView!!.itemAnimator = DefaultItemAnimator()
        recyclerView!!.adapter = mAdapter
    }

    override fun onClick(p0: View?) {
        /*if (p0!!.id == R.id.tvDate) {

        }*/
    }
}