package com.driver.crusie.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.driver.crusie.R
import com.driver.crusie.common.AppPreferenceManager

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
           /* if (AppPreferenceManager.getUser() != null
                && AppPreferenceManager.isOtpVerify()
            ) {
                startActivity(Intent(this, HomeActivity::class.java))
            } else {
                startActivity(Intent(this, LoginActivity::class.java))
            }*/
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }, 3000)
    }
}